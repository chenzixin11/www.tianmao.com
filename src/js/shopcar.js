import $ from "./lib/jquery.js";
import cookie from "./lib/cookie.js";

let shop = cookie.get("shop");

shop = JSON.parse(shop);

let idList = shop.map((el) => el.id).join();

$.ajax({
  type: "get",
  url: "../interface/shopcar.php",
  data: { idList },
  dataType: "json",
})
  .then((res) => {
    let tit = ``;

    res.forEach((el, i) => {
      let pic = JSON.parse(el.picture);
      let current = shop.filter((elm) => elm.id === el.id);

      let totall = parseInt(el.price) * parseInt(current[0].num);
      

      tit += ` <ul>
      <li class="th wp-1">
        <div class="td-inner">
          <div>
            <input type="checkbox" class="check" data-id="${el.id}">
          </div>
        </div>
      </li>
      <li class="th wp-2">
        <div class="td-inner">
          <div class="td-inner-1">
            <a href="">
              <img src="${pic[0].src}" alt="">
            </a>
          </div>
          <div class="td-inner-2">
            <div class="td-inner-2-1">
              <a href="">${el.title}</a>
            </div>
            <div class="td-inner-2-2">
              <div class="td-inner-2-2-1"></div>
              <div class="td-inner-2-2-2">
                <div>
                  <span><img src="./img/shopcar/xcard.png" alt=""></span>
                  <a href=""><img src="./img/shopcar/T1Vyl6FCBlXXaSQP_X-16-16.png" alt=""></a>
                  <a href=""><img src="./img/shopcar/T1BCidFrNlXXaSQP_X-16-16.png" alt=""></a>
                </div>
              </div>
              <div class="td-inner-2-2-3"></div>
            </div>
          </div>
        </div>
      </li>
      <li class="th wp-3">
        <div class="td-inner">
          <p>网络类型:5G全网通</p>
          <p>机身颜色:【荣耀x30】-幻夜黑</p>
          <p>套餐类型:官方标配</p>
          <p>存储容量:6+128GB</p>
        </div>
      </li>
      <li class="th wp-4">
        <div class="td-inner">
          <div><em>￥${(+el.price).toFixed(2)}</em></div>
        </div>
      </li>
      <li class="th wp-5">
        <div class="td-inner">
          <div>
            <a href="" class="td-inner-a1 reduce" id-date=${el.id}>-</a>
            <input type="number" value="${
              current[0].num
            }" min="1" class="text-amount">
            <a href="" class="td-inner-a2 add" id=${el.id}>+</a>
          </div>
        </div>
      </li>
      <li class="th wp-6">
        <div class="td-inner">
         <em>￥${(el.price * current[0].num).toFixed(2)}</em>
        </div>
      </li>
      <li class="th wp-7">
        <div class="td-inner">
         <a href="">移入收藏夹</a>
         <a href="javascript:;" class="removeitem" data-id="${el.id}">删除</a>
        </div>
      </li>
    </ul>`;
    });

    $(".cart-main-2-content").html(tit);
    $(".removeitem").on("click", function () {
      let res = shop.filter((el) => el.id != $(this).attr("data-id")); // 筛选被点击的元素
      cookie.set("shop", JSON.stringify(res)); // 剩余内容写回cookie
      location.reload(); // 刷新页面
    });

    $(".add").on("click", function () {
      let num = $(this).siblings(".text-amount").val();
      num++;
      $(this).siblings(".text-amount").val(num);
      let id = shop.filter((el) => el.id == $(this).attr("id"));

      addItem(id[0].id, $(".text-amount").val());
      location.reload();
    });

    $(".reduce").on("click", function () {
      let num = $(this).siblings(".text-amount").val();
      num--;
      if (num < 1) {
        num = 1;
      }
      $(this).siblings(".text-amount").val(num);
      let id1 = shop.filter((el) => el.id == $(this).attr("id-date"));
      reduceItem(id1[0].id, $(".text-amount").val());
      location.reload();
    });

    let ar = 0;
    let count = 0;
    let arr5 = [];
    $(".check").on("change", function () {
      let abc = $(this).attr("data-id");

      if (this.checked) {
        let arr = shop.filter((el) => el.id == abc);
        let arr1 = res.filter((el) => el.id == abc);
        let eee = parseInt(arr[0].num) * parseInt(arr1[0].price);
        ar += eee;
        arr5.push(this.checked);
        $("#btn1").css({
          cursor: "pointer",
          background: "red",
        });
        $("#btn2").css({
          cursor: "pointer",
          background: "red",
        });
      } else {
        let arr = shop.filter((el) => el.id == abc);
        let arr1 = res.filter((el) => el.id == abc);
        let eee = parseInt(arr[0].num) * parseInt(arr1[0].price);
        ar -= eee;
        arr5.pop();
        $("#btn1").css({
          cursor: "not-allowed",
          background: "#b0b0b0",
        });
        $("#btn2").css({
          cursor: "not-allowed",
          background: "#b0b0b0",
        });
      }
      $(".em1").html(ar);
      if (arr5.length == $(".check").length) {
        $(".ckck").prop("checked", true);
      } else {
        $(".ckck").prop("checked", false);
      }
    });

    $("#btn1").on("click", function () {
      alert("结算成功");
      cookie.set("shop", "", -1);
      location.reload();
    });

    $(".ckck").on("change", function () {
      if (this.checked) {
        $(".ckck").prop("checked", true);
        let arr = Array.from($(".check")).filter((el) => !el.checked);
        arr.forEach((e) => {
          $(e).prop("checked", true);
          $(e).trigger("change");
        });
      } else {
        $(".ckck").prop("checked", false);
        let arr = Array.from($(".check")).filter((el) => el.checked);
        arr.forEach((e) => {
          $(e).prop("checked", false);
        });
        $(".check").trigger("change");
      }
    });
  })
  .catch((xhr) => {
    console.log(xhr.status);
  });

function addItem(id, num) {
  let product = {
    id,
    num,
  };
  let shop = cookie.get("shop"); //从cookie获取数据

  if (shop) {
    shop = JSON.parse(shop);
    if (shop.some((el) => el.id == id)) {
      let index = shop.findIndex((elm) => elm.id == id); // 获得商品对象在数组中的索引
      let count = parseInt(shop[index].num);
      count++;
      shop[index].num = count;
    }
    cookie.set("shop", JSON.stringify(shop));
  }
}

function reduceItem(id, num) {
  let product = {
    id,
    num,
  };
  let shop = cookie.get("shop"); //从cookie获取数据

  if (shop) {
    shop = JSON.parse(shop);
    if (shop.some((el) => el.id == id)) {
      let index = shop.findIndex((elm) => elm.id == id); // 获得商品对象在数组中的索引
      let count = parseInt(shop[index].num);
      count--;
      shop[index].num = count;
    }
    cookie.set("shop", JSON.stringify(shop));
  }
}
