import $ from "./lib/jquery.js";
import cookie from "./lib/cookie.js";

let id = location.search.split("=")[1];
console.log(id);

$.ajax({
  type: "get",
  url: "../interface/getitem.php",
  data: { id },
  dataType: "json",
})
  .then((res) => {
    let pic = JSON.parse(res.picture);

    $("#de1>img").attr("src", pic[0].src);
    $(".big-box>img").attr("src", pic[0].src);

    let pict = JSON.parse(res.details);
    let pict1 = ``;
    pict1 += `<div class="content-1">
    <span
      >温馨提示:ZP-OL 水感透白光曜精华露14ml 近效期 2023/4/1</span
    >
    <img
      src="${pict[0].src}"
      alt=""
    />
    <img
      src="${pict[1].src}"
      alt=""
    />
  </div>`;

    let tl = `<h1>${res.title}</h1>`;

    let price = `<em class="tm-yen">¥</em>
    <span class="tm-price">${res.price}</span>`;

    $(".tb_detail-hdbox").html(tl);
    $(".dd1").html(price);

    $(".ke-post").html(pict1);

    $("#addItem").on("click", function () {
      addItem(res.id, $("#num").val());
      // alert(1);
    });
  })
  .catch((xhr) => {
    console.log(xhr.status);
  });

function addItem(id, num) {
  let product = { id, num };

  let shop = cookie.get("shop"); // 从cookie中获得数据

  if (shop) {
    // 判断是否获得到数据
    shop = JSON.parse(shop);

    // 当商品id在cookie数据中已经存在时 需要修改数量 而不是添加商品
    if (shop.some((el) => el.id == id)) {
      let index = shop.findIndex((elm) => elm.id == id); // 获得商品对象在数组中的索引
      let count = parseInt(shop[index].num);
      count += parseInt(num);
      shop[index].num = count;
    } else {
      shop.push(product);
    }
  } else {
    shop = [];
    shop.push(product);
  }

  cookie.set("shop", JSON.stringify(shop)); // 将数组转换成JSON字符串存入cookie
}

$("#addItem").on("click", function () {
  alert("加入了购物车");
});
