import $ from "./lib/jquery.js";

// import { Swiper } from "../swiper-7.4.1/swiper/swiper-bundle.esm.browser.js";
var swiper = new Swiper(".mySwiper", {
  spaceBetween: 30,
  loop: true,
  autoplay: {
    delay: 3000,
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});




$.ajax({
  type: "get",
  url: "../interface/getitems.php",
  dataType: "json",
}).then((res) => {
  let template = "";
  res.forEach((el) => {
    let pic = JSON.parse(el.picture);
    //console.log(pic);

    template += `<div class="recommendation-img">
        <a href="http://localhost/2206/www.tianmao.com/src/details.html?id=${el.id}">
          <img src=" ${pic[0].src}" alt="" />
          <div></div>
          <p class="recommendation-img-1">${el.title}</p>
          <p class="recommendation-img-2">￥${el.price}</p>
        </a>
      </div>`;
  });

  $(".recommendation-imgs").html(template);
});
